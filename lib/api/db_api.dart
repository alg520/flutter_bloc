
import 'package:flutter_bloc/models/user.dart';

class DbApi {
  Future<User> getUser() async {
    await Future.delayed(Duration(seconds: 3));
    User _user = User('Ben', 35, 172.0);
    return _user;
  }
}

DbApi api = DbApi();