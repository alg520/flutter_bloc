import 'package:flutter/material.dart';
import 'dart:async';

class StreamCounterPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => StreamCounterPageState();
}

class StreamCounterPageState extends State<StreamCounterPage> {

  final StreamController<int> _streamController = StreamController<int>();
  int _count = 0;

  @override
  void dispose() {
    _streamController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Bloc Demo'),
        elevation: 0.0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('You have pushed the button htis many times:'),
            StreamBuilder<int>(
              stream: _streamController.stream,
              initialData: 0,
              builder: (context, snapshot){
                return Text('${snapshot.data}', style: Theme.of(context).textTheme.display1,);
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          _streamController.sink.add(++_count);
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}