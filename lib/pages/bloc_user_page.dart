import 'package:flutter/material.dart';
import 'package:flutter_bloc/blocs/user_bloc.dart';
import 'package:flutter_bloc/models/user.dart';
import 'package:flutter_bloc/widgets/bloc_provider.dart';

class BlocUserPage extends StatefulWidget {
  @override
  _BlocUserPageState createState() => _BlocUserPageState();
}

class _BlocUserPageState extends State<BlocUserPage> {

  String _newName;

  @override
  Widget build(BuildContext context) {
    final UserBloc userBloc = BlocProvider.of<UserBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('User Bloc'),
      ),
      body: StreamBuilder<User>(
        stream: userBloc.outUser,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    '${snapshot.data.name}',
                    style: Theme.of(context).textTheme.display1,
                  ),
                  TextField(
                    onChanged: (value) => _newName = value,
                    decoration: InputDecoration(labelText: snapshot.data.name),
                  ),
                ],
              ),
            );
          }
          return LinearProgressIndicator();
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          userBloc.updateName(_newName);
        },
        tooltip: 'Save',
        child: const Icon(Icons.save),
      ),
    );
  }
}
