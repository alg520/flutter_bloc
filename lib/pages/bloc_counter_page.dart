import 'package:flutter/material.dart';
import 'package:flutter_bloc/blocs/counter_bloc.dart';
import 'package:flutter_bloc/widgets/bloc_provider.dart';

class BlocCounterPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() => BlocCounterPageState();
}

class BlocCounterPageState extends State<BlocCounterPage> {

  @override
  Widget build(BuildContext context) {
    final CounterBloc counterBloc = BlocProvider.of<CounterBloc>(context);
    return Scaffold(
      appBar: AppBar(
        title: Text('Bloc Demo'),
        elevation: 0.0,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text('You have pushed the button htis many times:'),
            StreamBuilder<int>(
              stream: counterBloc.outCounter,
              initialData: 0,
              builder: (context, snapshot){
                return Text('${snapshot.data}', style: Theme.of(context).textTheme.display1,);
              },
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          counterBloc.incrementCounter.add(1);
        },
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}