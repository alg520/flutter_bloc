import 'package:flutter/material.dart';
import 'package:flutter_bloc/blocs/user_bloc.dart';
import 'package:flutter_bloc/pages/bloc_user_page.dart';
import 'widgets/bloc_provider.dart';
// import 'pages/bloc_counter_page.dart';
// import 'blocs/counter_bloc.dart';

//import 'pages/stream_counter_page.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'bloc_pattern_tutorial',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
//      home: StreamCounterPage(),
      home: BlocProvider(
          child: BlocUserPage(),
          bloc: UserBloc()
      ),
      // home: BlocProvider(
      //     child: BlocCounterPage(),
      //     bloc: CounterBloc()
      // ),
    );
  }
}
