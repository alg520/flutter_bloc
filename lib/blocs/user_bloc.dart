
import 'dart:async';

import 'package:flutter_bloc/models/user.dart';
import 'package:flutter_bloc/widgets/bloc_provider.dart';
import 'package:flutter_bloc/api/db_api.dart';

class UserBloc implements BlocBase {

  User _user;

  StreamController<User> _userController = StreamController<User>.broadcast();
  Sink<User> get _inUser => _userController.sink;
  Stream<User> get outUser => _userController.stream;

  @override
  void dispose() {
    _userController.close();
  }

  UserBloc(){
    init();
  }

  void init() async {
    _user = await api.getUser();
    _inUser.add(_user);
  }

  void updateUser(User user) {
    _user = user;
    _inUser.add(_user);
  }

  void updateName(String name){
    _user.name = name;
    _inUser.add(_user);
  }
}